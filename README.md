## Lineaminetos de java

## Nomenclatura de Clases e Interfaces para Java

Los nombres de las clases deben ser sustantivos, en mayúsculas y minúsculas, con la primera letra de cada palabra interna en mayúscula. El nombre de las interfaces también debe estar en mayúscula (la primera) al igual que los nombres de las clases. Use palabras completas y debe evitar acrónimos y abreviaturas.

ejemplo:
```cs
Interface  Bicycle{}
Class MountainBike implements Bicyle{}

Interface Sport{}
Class Football implements Sport{}
```
## Nomenclatura de metodos para Java

Para nombrar los metodos en Java se debe usar el estilizado de Camel Case, en donde se debe diferenciar cada palabra con una mayuscula.

ejemplo:
```cs
StudentManager() , CarController() , numberOfStudents() , runAnalysis() , etc.
```
## Nomenclatura de pruebas Unitarias

Para nombrar las clases y metodos de prueba nos debemor referir a lo antes mencionado en las clases  y metodos. 

## Nomenclatura para Setters y Getters
El esquema de nomenclatura de setter y getter debe seguir las convenciones de nomenclatura de Javabean como getXxx () y setXxx (), donde es el nombre de la variable Xxx. 

ejemplo:
```cs
public void setName(String name){...}, public String getName(){...}
```

## Nomenclatura para declaración de variables public , private , protected 
Se recomienda usar la declaración con inicial en minuscula y ingles
```cs
public String name;
private int age;
protected float salary;
public namePerson;
```
Inicializar las variables
```cs
name = "hello";
age = 23;
salary = 100.000f;
namePerson = "Duvan leal"
```
## Nomenclatura para declaración de Interfaces
La declaracion de una interfaz es similar a la de una clase
se debe utilizar **Pascal Case** para el nombre y contiene 
metodos vacios
```cs
public interface Person {
    public void work (); 
}
```
**Implementación interfaz**

Se invoca el metodo de la interfaz y se implementa.
```cs
public class Engineer implements Person{
  public void work() {
    System.out.println("is working")
  }
}
```
## Nomenclatura declaración de constructor
El constructor debe llevar el mismo nombre de la clase.
Es usado para inicializar variables
```cs
public class Main {
  int x;  // crea un atributo de clase

  // crea el constructor en la clase main
  public Main() {
    x = 5;  // Establece el valor inicial de la clase atributo x
  }

  public static void main(String[] args) {
    Main myObj = new Main(); // Crea un objeto de la clase main(Esto para llamar al constructor)
    System.out.println(myObj.x); // imprime en pantalla el valor de X
  }
}
```
## Nomenclatura para declaracion de ciclos for, while , do-while
Se debe usar **for** cuando se tenga conocimiento de cuantas veces corre el bloque de codigo
```cs
for (int i = 0; i < 5; i++) {
  System.out.println(i);
}
```
El bucle **while** recorre un bloque de código siempre que una condición específica sea **true**
```cs
int i = 1;
while (i < 5) {
  System.out.println(i);
  i++;
}
```
El bucle **do-while** ejecuta el bloque de codigo almenos una vez antes de validar la condición
```cs
int i = 0;
do {
  System.out.println(i);
  i++;
}
while (i < 5);
```
## Condiciones de Java y sentencias If

Java admite las siguientes condiciones logícas habituales
```cs
a < b // es mayor
b > a  // es menor
b = a  // es igual a
b >= a // mayor o igual
b <= a // menor o igual
b != a // no es igual
```

**declaración if**

La declaracion **if** se utiliza para es especificiar un bloque 
de código java, ejecutando si la condicion es **true**
```cs
if (a > b ) { //declarar la sentencia if en minúscula
  System.out.println("a es mayor que b!!");
}
```
**declaración else**

La declaración **else** especifica un bloque de codigo , ejecutando
si la condicion es **false**
```cs
if (a > b ) {
  System.out.println("a es mayor que b!!");
} else { //declarar la sentencia else en minúscula
  System.out.println("a No es mayor que b!!");
}
```

**declaración else if**

La declaración else if especifica una nueva condicion , si la 
primera condición es **false**

```cs
int a = 10;
if (a < 5) {
  System.out.println("a es mayor");
} else if (a < 9) {
  System.out.println("a es aun mayor");
} else {
  System.out.println("a es aun menor");
}
